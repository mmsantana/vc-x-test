class Queue:
    def __init__(self):
        self.data = ''
        self.final_value = ''

    def __str__(self):
        for i in range(self.size()):
            if i == 0:
                self.final_value += str(self.data[i])
            else:
                self.final_value += ' -> ' + str(self.data[i])
        return str(self.final_value)

    def __len__(self):
        return len(self.data)

    def is_empty(self):
        if len(self.data) == 0:
            return True
        else:
            return False

    def enqueue(self, value_to_add):
        if type(value_to_add) is int or type(value_to_add) is str:
            self.data = self.data + str(value_to_add)
        elif callable(value_to_add):
            self.data = self.data + 'F'
        elif type(value_to_add) is dict:
            self.data = self.data + 'D'
        elif type(value_to_add) is list:
            self.data = self.data + 'L'
        elif value_to_add is None:
            self.data = self.data + 'N'
        return self.data

    def dequeue(self):
        if self.is_empty():
            raise QueueDequeueException('Não é possível remover itens de uma lista vazia!')
        else:
            value_to_pop = self.data[0]
            self.data = self.data[1:]
        return int(value_to_pop)

    def size(self):
        return len(self.data)


class QueueDequeueException(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)
