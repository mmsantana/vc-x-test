class Node:
    def __init__(self, data):
        self.data = data
        self.next_node = None

    def __str__(self):
        return str(self.data)


class Queue:
    def __init__(self):
        self.first_element = None
        self.last_element = None

    def __len__(self):
        count = 0
        current_node = self.first_element
        while current_node is not None:
            count += 1
            current_node = current_node.next_node
        return count

    def __str__(self):
        string = ''
        current_node = self.first_element
        while current_node is not None:
            string += str(current_node.data) + ' -> '
            current_node = current_node.next_node
        return string[:-4]

    def enqueue(self, value):
        new_node = Node(value)
        if self.is_empty():
            self.first_element = self.last_element = new_node
        else:
            self.last_element.next_node = new_node
            self.last_element = new_node

    def dequeue(self):
        if self.is_empty():
            raise QueueDequeueException('Não é possível remover itens de uma pilha vazia!')
        first_element = self.first_element.data
        if self.first_element == self.last_element:
            self.first_element = self.last_element = None
        else:
            self.first_element = self.first_element.next_node
        return first_element

    def is_empty(self):
        return self.first_element is None


class QueueDequeueException(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return str(self.value)
