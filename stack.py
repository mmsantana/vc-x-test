class Stack:
    def __init__(self):
        self.data = ''
        self.final_value = ''

    def __str__(self):
        for i in range(self.size()):
            if i == 0:
                self.final_value += str(self.data[i])
            else:
                self.final_value += ' -> ' + str(self.data[i])
        return str(self.final_value)

    def __len__(self):
        return len(self.data)

    def is_empty(self):
        if len(self.data) == 0:
            return True
        else:
            return False

    def push(self, value_to_add):
        if type(value_to_add) is int or type(value_to_add) is str:
            self.data = str(value_to_add) + self.data
        elif callable(value_to_add):
            self.data = 'F' + self.data
        elif type(value_to_add) is dict:
            self.data = 'D' + self.data
        elif type(value_to_add) is list:
            self.data = 'L' + self.data
        elif value_to_add is None:
            self.data = 'N' + self.data
        return self.data

    def add(self, value_to_add):
        if type(value_to_add) is int or type(value_to_add) is str:
            self.data = str(value_to_add) + self.data
        elif callable(value_to_add):
            self.data = 'F' + self.data
        elif type(value_to_add) is dict:
            self.data = 'D' + self.data
        elif type(value_to_add) is list:
            self.data = 'L' + self.data
        elif value_to_add is None:
            self.data = 'N' + self.data
        return self.data

    def pop(self):
        if self.is_empty():
            raise StackPopException('Não é possível remover itens de uma pilha vazia!')
        else:
            value_to_pop = self.data[0]
            self.data = self.data[1:]
        return int(value_to_pop)

    def size(self):
        return len(self.data)


class StackPopException(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)
