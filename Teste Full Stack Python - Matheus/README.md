# Como executar o script

Para executar os testes faça o seguinte procedimento:

- Abra o terminal na pasta do arquivo tests.py
- Digite o código seguinte:
python -m unittest tests.py

No terminal será indicado o número de testes que o script passou e as respectivas falhas ou erros.
Caso seja de interesse ler o conteúdo dos arquivos, clique com o botão direito sobre o arquivo e escolha o editor de texto de preferência.

Obs: No script tests.py foi realizada uma alteração, ao invés de utilizar a função 'print()' para comparar o output da classe com as strings, foi utilizada a função 'str()' para tal comparação.