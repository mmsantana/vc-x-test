# -*- coding: utf-8 -*-

# NOTE (@eduardo): vi que você utilizou uma string para criar a estrutura de dados de armazenamento
# da fila. Por mais que funcione, não é o recomendável. Você deveria ter utilizado uma estrutura
# de lista encadeada, por exemplo.
# Como seu background não é em Ciências da Computação, vou relevar esse problema visto que seu
# código funciona e não foi utilizado listas, dicionários, e outras estruturas
# builtin do Python que facilitariam sua vida.
#
class Queue:
    def __init__(self):
        self.data = ''
        self.final_value = ''

    def __str__(self):
        # NOTE (@eduardo):
        # Apesar da lógica estar correta para a resolução do problema, você poderia ter resolvido
        # o mesmo problema de uma maneira mais legível e mais fácil.
        # Um exemplo seria uma unica linha:
        # return ' -> '.join([item for item in queue])
        #
        # Para fazer iso você teria que implementar um método __iter__ na classe.
        # Como .size() faz faz a mesma coisa que len, você poderia ter utilizado apenas len que é
        # mais legível também.
        for i in range(self.size()):
            if i == 0:
                self.final_value += str(self.data[i])
            else:
                self.final_value += ' -> ' + str(self.data[i])
        return str(self.final_value)

    def __len__(self):
        return len(self.data)

    def is_empty(self):
        # NOTE (@eduardo): você não precisava fazer esse if e else dessa maneira. Quando você tem
        # um 0 como inteiro, Python interpreta ele como False se utilizado em
        # estruturas de comparação.
        #
        # Essa função poderia ser feita da seguinte forma:
        # return bool(len(self.data))

        # NOTE (@eduardo): Quando um if tem um retorno e somente um else, não é preciso criar esse
        # else.  Se você não quisesse utilizar o oneliner descrito acima, você poderia ter feito:
        #
        # if len(self.data):
        #   return True
        # return False
        #
        # Ou mesmo:
        # return True if len(self.data) else False
        #
        # Mas lembre-se que o melhor jeito seria o descrito na primeira nota.
        if len(self.data) == 0:
            return True
        else:
            return False

    def enqueue(self, value_to_add):
        # NOTE(@eduardo): não entendi essa parte de código.
        # Você transforma diferentes tipos de entrada na fila em letras e depois em dequeue
        # você sempre transforma o item retirado em inteiro. Esse código não tem como funcionar.
        # Um exemplo:
        # >>> q = Queue()
        # >>> q.enqueue({})
        # >>> q.dequeue()
        # ValueError: invalid literal for int() with base 10: 'D'
        #
        # O correto seria a fila me devolver o objeto enviado anteriormente.
        # Me parece que você esqueceu ou desistiu dessa parte código.
        # Gostaria que você me explicasse por e-email o que você quis fazer nessa parte de código.
        if type(value_to_add) is int or type(value_to_add) is str:
            self.data = self.data + str(value_to_add)
        elif callable(value_to_add):
            self.data = self.data + 'F'
        elif type(value_to_add) is dict:
            self.data = self.data + 'D'
        elif type(value_to_add) is list:
            self.data = self.data + 'L'
        elif value_to_add is None:
            self.data = self.data + 'N'
        return self.data

    def dequeue(self):
        if self.is_empty():
            # NOTE (@eduardo): como só há o raise nesse if, o código termina aqui então não há 
            # necessidade de escrever o else. Você poderia ter feito da seguinte maneira:
            #
            # if self.is_empty():
            #   raise QueueDequeueException('Não é possível remover itens de uma lista vazia!')
            # ... # continuação do código. deixa a leitura melhorar
            #
            # Esse estilo de programação tem o nome de early return.
            raise QueueDequeueException('Não é possível remover itens de uma lista vazia!')
        else:
            value_to_pop = self.data[0]
            self.data = self.data[1:]

        # NOTE (@eduardo): Isso não faz sentido. Vide nota na função enqueue
        return int(value_to_pop)

    def size(self):
        return len(self.data)


class QueueDequeueException(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)
