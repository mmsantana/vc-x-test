class Node:
    def __init__(self, data):
        self.data = data
        self.next_node = None

    def __str__(self):
        return str(self.data)


class Stack:
    def __init__(self):
        self.first_element = None
        self.last_element = None

    def __len__(self):
        count = 0
        current_node = self.first_element
        while current_node is not None:
            count += 1
            current_node = current_node.next_node
        return count

    def push(self, value):
        new_node = Node(value)
        if self.is_empty():
            self.first_element = self.last_element = new_node
        else:
            new_node.next_node = self.first_element
            self.first_element = new_node

    def add(self, value):
        return self.push(value)

    def pop(self):
        if self.is_empty():
            raise StackPopException('Não é possível remover itens de uma pilha vazia!')
        first_element = self.first_element.data
        if self.first_element == self.last_element:
            self.first_element = self.last_element = None
        else:
            self.first_element = self.first_element.next_node
        return first_element

    def is_empty(self):
        return self.first_element is None

    def __str__(self):
        if self.is_empty():
            return ''
        current_node = self.first_element
        string = ''
        while current_node is not None:
            string += str(current_node.data) + ' -> '
            current_node = current_node.next_node
        return string[:-4]


class StackPopException(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return str(self.value)
